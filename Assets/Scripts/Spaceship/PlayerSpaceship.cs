using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class PlayerSpaceship : Basespaceship, IDamagable
    {
        private PlayerSpaceship Instance { get; set; }
        public event Action OnExploded;

        
        // [SerializeField] private HealthBarScrip _healthBar;
        private void Awake()
        {
            audioSource = GetComponent<AudioSource>();
            
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
            
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            //SoundManager.Instance.Play(audioSource,SoundManager.Sound.PlayerFire);
            SoundManager.Instance.Play(SoundManager.Sound.PlayerFire);
            //var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            var bullet = PoolManager.Instance.GetPooledObject(PoolManager.PoolObjectType.PlayerBullet);
            if (bullet)
            {
                bullet.transform.position = gunPosition.position;
                bullet.transform.rotation = Quaternion.identity;
                bullet.SetActive(true);
                bullet.GetComponent<Bullet>().Init(Vector2.up);                
            }            
            
        }

        public void TakeHit(int damage)
        {
            // _healthBar.SetMaxHealth(Hp);
            Hp -= damage;
            // _healthBar.SetHealth(Hp);
            if (Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            //SoundManager.Instance.Play(audioSource,SoundManager.Sound.PlayerExplode);
            SoundManager.Instance.Play(SoundManager.Sound.PlayerExplode);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            Destroy(gameObject);
            OnExploded?.Invoke();
        }
    }
}
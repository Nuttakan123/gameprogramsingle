﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using Spaceship;
using UnityEngine;

public class GameManagerBoss : MonoBehaviour
{
    [SerializeField] private PlayerSpaceship _player;
    [SerializeField] private EnemySpaceship _enemy;
    [SerializeField] private GameManager _gameManager;

    private void Awake()
    {
        Debug.Assert(_player != null, "player cannot be null");
        Debug.Assert(_enemy != null, "enemy cannot be null");
        Debug.Assert(_gameManager != null, "gameManager cannot be null");
    }

    private void Start()
    {
        Instantiate(_player);
        Instantiate(_enemy);
    }
}

﻿using System;
using Spaceship;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Manager
{
    public class ScoreManager : MonoBehaviour
    {
        public event Action OnScoreUpdated;
        public int playerScore;
        public static ScoreManager Instance { get; private set; }

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);                
            }
        }
        
        private void Start()
        {
            playerScore = 0;
        }

        public int GetScore()
        {
            return playerScore;
        }
        
        public void UpdateScore(int score)
        {
            playerScore += score;
            
            if (playerScore == 15)
            {
                Time.timeScale = 0f;
                EnemySpaceship.Hp += 100;
                SceneManager.LoadScene("Next");
            }
            if (playerScore >= 16)
            {
                playerScore += 8;
            }
           
            OnScoreUpdated?.Invoke();
           
        }

        private void ResetScore()
        {
            playerScore = 0;
            
        }
        
        private void OnRestarted()
        {
            ResetScore();
        }
    }
}



﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class SeceneManager : MonoBehaviour
    {
        public void LoadScene(string name)
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(name);
        }

        public void ExitScene()
        {
            Application.Quit();
        }
    }
}

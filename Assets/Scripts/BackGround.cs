﻿using UnityEngine;

namespace BackGound
{
    public class BackGround : MonoBehaviour
    {
    
        [SerializeField] private float speed = 1.78f;
        private Vector3 startPosition;

        private void Start()
        {
            startPosition = transform.position;
        }

        private void Update()
        {
                
            transform.Translate(Vector3.down * speed * Time.deltaTime);
            if (transform.position.y <= - 11.1F)
            {
                transform.position = startPosition;
            }
        }
    }
}

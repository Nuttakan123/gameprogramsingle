﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using TMPro;
using UnityEngine;


public class InterFaceScore : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _textScore;
    [SerializeField] private TextMeshProUGUI _textWinner;
    

    private void Start()
    {
        ScoreManager.Instance.OnScoreUpdated += Score;
    }
    private void Score()
    {
        _textScore.text = $"Score : {ScoreManager.Instance.GetScore()}";
        if (ScoreManager.Instance.GetScore() > 200)
        {
            _textWinner.text = $"YOU ARE WINNER Score : {ScoreManager.Instance.GetScore()}";
            Time.timeScale = 0f;
        }
    }
}

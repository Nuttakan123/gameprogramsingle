﻿using System.Collections;
using System.Collections.Generic;
using Manager;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CountDown : MonoBehaviour
{
    [SerializeField] public double timeCountDown;
    [SerializeField] private Text countDownDisplay;
    
    void Start()
    {
        StartCoroutine(TimeCountDownToStart());
        
    }
        
    IEnumerator TimeCountDownToStart()
    {
        while (timeCountDown > 0)
        {
            countDownDisplay.text = timeCountDown.ToString();
            yield return new WaitForSeconds(1f);
            timeCountDown--;
        }
            
        if (timeCountDown == 0)
        {
            countDownDisplay.text = "Time Out ";
            yield return new WaitForSeconds(2f);
            Time.timeScale = 0f;
            SceneManager.LoadScene("Game");
        }
        
    }
}
